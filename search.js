var inputFile = "input.txt";

function findWord(grid, word) {
  const rows = grid.length;
  const columns = grid[0].length;

  for (let r = 0; r < rows; r++) {
    for (let c = 0; c < columns; c++) {
      if (word[0] === grid[r][c]) {
        const directions = [
          [-1, -1],
          [-1, 0],
          [-1, 1],
          [0, -1],
          [0, 1],
          [1, -1],
          [1, 0],
          [1, 1],
        ];
        for (let i = 0; i < directions.length; i++) {
          const [x, y] = directions[i];
          searchDirection(grid, word, r, c, x, y, rows, columns);
        }
      }
    }
  }
}

function searchDirection(grid, word, r, c, dirR, dirC) {
  const maxRows = grid.length - 1; // Calculate maximum rows/cols outside of loop
  const maxCols = grid[0].length - 1;

  for (let i = 1; i < word.length; i++) {
    const row = r + i * dirR; // Calculate new row and column values
    const col = c + i * dirC;

    if (row < 0 || row > maxRows || col < 0 || col > maxCols) {
      return;
    }
    if (grid[row][col] != word[i]) {
      return;
    }
  }
  // Output the word along with start and end coordinates
  console.log(
    word +
      " " +
      r +
      ":" +
      c +
      " " +
      (r + (word.length - 1) * dirR) +
      ":" +
      (c + (word.length - 1) * dirC)
  );
}

const fs = require("fs");

fs.readFile(inputFile, "utf8", (err, data) => {
  if (err) throw err;

  const lines = data.split("\n").map((line) => line.trim());

  const [numRows] = lines
    .shift()
    .split("x")
    .map((n) => Number(n));

  const grid = lines.slice(0, numRows).map((line) => line.split(" "));

  const words = lines.slice(numRows);

  words.forEach((word) => findWord(grid, word));
});
